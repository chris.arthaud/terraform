provider "aws" {
  profile    = "default"
  region     = "eu-west-1"
}

resource "aws_instance" "example" {
  ami           = "ami-028188d9b49b32a80"
  instance_type = "t2.micro"
  
  provisioner "local-exec" {
    command = "echo ${aws_instance.example.public_ip} > aws_example_ip.txt"
  }
}

output "public_ip" {
  value = aws_instance.example.public_ip
}
